local MapCell = class 'MapCell' {
	init = function(self)
		self.objects = {}
		self.flags   = {
			terrain = nil,
			opaque  = nil,
			explored= nil,
		}
	end,
}
return MapCell