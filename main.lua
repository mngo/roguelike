function love.run()

	math.randomseed(os.time())
	math.random() math.random()
	
    if love.math then
        love.math.setRandomSeed(os.time())
    end

    if love.event then
        love.event.pump()
    end

    if love.load then love.load(arg) end

    -- We don't want the first frame's dt to include time taken by love.load.
    if love.timer then love.timer.step() end

    local dt = 0

    -- Main loop time.
    while true do
        -- Process events.
        if love.event then
            love.event.pump()
            for e,a,b,c,d in love.event.poll() do
                if e == "quit" then
                    if not love.quit or not love.quit() then
                        if love.audio then
                            love.audio.stop()
                        end
                        return
                    end
                end
                love.handlers[e](a,b,c,d)
            end
        end

        -- Update dt, as we'll be passing it to update
        if love.timer then
            love.timer.step()
            dt = love.timer.getDelta()
        end

        -- Call update and draw
        if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled

        if love.window and love.graphics and love.window.isCreated() then
            love.graphics.clear()
            love.graphics.origin()
            if love.draw then love.draw() end
            love.graphics.present()
        end

        if love.timer then love.timer.sleep(0.001) end
    end

end

function love.load()

	-----------------------------
	-- GLOBALS
	-----------------------------

	-- DEBUG_REVEAL_ALL      = true
	-- DEBUG_IGNORE_COLLISION= true
	
	require 'strict'

	global 'enum'
	global 'class'
	global 'common'
	
	class = require 'class'
	
	-----------------------------
	-- INITIALIZATION
	-----------------------------
	local gamestate = require 'gamestate'
	local ingame    = require 'ingame'
	gamestate.register()
	gamestate.switch(ingame)
	
end