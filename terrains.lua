local colors = require 'colors'

local terrains = {
	--------------------------
	-- List of terrains
	--------------------------
	[1] =
	{
		name = 'nothing', 
		character = '\0', 
		foreground = colors.foreground,
		background = colors.background,
		solid = false,
	},
	[2] = {
		name = 'stone wall', 
		character = '#', 
		foreground = colors.white,
		background = colors.light_black,
		solid = true,
	},
	[3] = {
		name = 'floor', 
		character = '.', 
		foreground = colors.white,
		background = colors.background,
		solid = false,
	},
	[4] = {
		name = 'stairs up', 
		character = '<', 
		foreground = colors.yellow,
		background = colors.background,
		solid = false,
	},
	[5] = {
		name = 'stairs down', 
		character = '>', 
		foreground = colors.yellow,
		background = colors.background,
		solid = false,
	},
}

return terrains