local alphabet      = 'abcdefghijklmnopqrstuvwxyz'
local capslock_keys = {}
local printable_keys= {}

for letter in alphabet:gmatch '.' do
	capslock_keys[letter] = true
end

for i = 32,126 do
	printable_keys[string.char(i)] = true
end


local input = {
	default_map     = {},
	shift_map       = {},
	ctrl_map        = {},
	alt_map         = {},
	onTextInput     = nil,
	_repeat_key     = '',
	_repeat_text    = nil,
	_repeat_delay   = 0.3,
	_repeat_interval= 0.1,
	_time           = 0,
	_is_repeating   = false,
	
	setRepeatDelay = function(self,delay)
		self._repeat_delay = delay
	end,
	setRepeatInterval = function(self,period)
		self._repeat_interval = period
	end,
	keypressed = function(self,key)
		local callback
		
		-- Check capslock first
		if love.keyboard.isDown 'capslock' and capslock_keys[key] and not self:isDownShift() then
			callback = self.shift_map[key]
		elseif self:isDownShift() then
			callback = self.shift_map[key]
		elseif self:isDownCtrl() then
			callback = self.ctrl_map[key]
		elseif self:isDownAlt() then
			callback = self.alt_map[key]
		else
			callback = self.default_map[key]
		end
	
		if callback then callback() end
		if self._repeat_key ~= key then 
			self._time        = 0 
			self._is_repeating= false
		end
		self._repeat_key = key
	end,
	keyreleased = function(self,key)
		if key == self._repeat_key then
			self._time        = 0 
			self._is_repeating= false
		end
	end,
	textinput = function(self,text)
		self._repeat_text = text
		if self.onTextInput then self:onTextInput(text) end
	end,
	_repeatInput = function(self)
		love.keypressed(self._repeat_key)
		if printable_keys[self._repeat_key] then
			love.textinput(self._repeat_text)
		end
	end,
	update = function(self,dt)
		if love.keyboard.isDown(self._repeat_key) then
			self._time = self._time + dt
			if self._is_repeating then
				if self._time > self._repeat_interval then
					self._time = 0
					self:_repeatInput()
				end
			elseif self._time > self._repeat_delay then
				self._time        = 0
				self._is_repeating= true
				self:_repeatInput()
			end
		end
	end,
	
	-- CHECK MODIFIER STATE
	
	isDownShift = function(self)
		return love.keyboard.isDown 'lshift' or love.keyboard.isDown 'rshift'
	end,
	isDownAlt = function(self)
		return love.keyboard.isDown 'ralt' or love.keyboard.isDown 'lalt'
	end,
	isDownCtrl = function(self)
		return love.keyboard.isDown 'rctrl' or love.keyboard.isDown 'lctrl'
	end,

}

return input
