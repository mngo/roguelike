local msg_history

local string_lines = function(self)
	local index = 1
	local len   = #self
	return function()
		if index > len then return end
		local i,j,line = self:find( '([^\r\n]*)\r?\n?', index )
		index = j+1
		return line
	end
end

-------------------
-- MODULE
-------------------

msg_history = {
	max_lines  = 20,
	line_width = 1000,
	lines      = {}
}

function msg_history:clear()
	self.lines = {}
end

function msg_history:iterSplit(str)
	local index = 1
	local lw    = self.line_width
	return function()
		local sub = str:sub(index,index+lw-1)
		if sub ~= '' then 
			index = index + lw
			return sub 
		end
	end
end

function msg_history:write(str)
	for line in string_lines(str) do
		for newline in self:iterSplit(str) do
			table.insert(self.lines,newline)
		end
	end
	
	-- hack for empty string
	if str == '' then table.insert(self.lines,str) end
	
	local lines = #self.lines
	while lines > self.max_lines do
		lines = lines - 1
		table.remove(self.lines,1)
	end
end

function msg_history:iterate()
	local lines     = self.lines
	local next_index= 1
	local delta     = 1
	return function()
		local index = next_index
		local line  = lines[index]
		if not line then return end
		next_index  = next_index + delta
		return index,line
	end
end

return msg_history