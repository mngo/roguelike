local factories = require 'factories'
local width     = 40
local height    = 40
local random    = love.math.random

-- 0.5 is middle split
local min_ratio      = .4 -- the lower bound ratio for splitting
local max_ratio      = .6 -- the upper bound ratio for splitting
local max_split_count= 5  -- how deep and many times one can split
local wall_prob      = .8 -- probability to make wall at split
local min_size       = 3 -- 1: 1x1, 3:3x3 (minimum room size including walls)

-- bsp 
return function(ingame)
	local map   = factories.newMap(width,height)
	local rooms = {}
	
	local function splitRoom(x,y,x2,y2,level)
		level = level or 1
		-- Split the room
		if level <= max_split_count then
			-- choose a random direction to split on
			local direction = random(1,2)
			if direction == 1 then -- split horizontally
				local d       = y2-y
				local min,max = math.floor(min_ratio*d),math.floor(max_ratio*d)
				local splity  = random(y+min,y+max)
				
				-- don't split if the new rooms are too small
				if splity-(y-1) < min_size or y2-(splity-1) < min_size then
					return
				end
				
				if x ~= x2 then
					repeat
						local has_exit
						local yi = splity
						for xi = x+1,x2-1 do
							local prob = random()
							if prob < wall_prob then
								map:setCellFlag(xi,yi, 'terrain', 2)
								map:setCellFlag(xi,yi, 'opaque', true)
							else
								has_exit = true
								map:setCellFlag(xi,yi, 'terrain', 3)
								map:setCellFlag(xi,yi, 'opaque', false)
							end
						end
					until has_exit
				end
				
				splitRoom(x,y, x2,splity,level+1)
				splitRoom(x,splity, x2,y2,level+1)
			else -- split vertically
				local d       = x2-x
				local min,max = math.floor(min_ratio*d),math.floor(max_ratio*d)
				local splitx  = random(x+min,x+max)
				
				if splitx-(x-1) < min_size or x2-(splitx-1) < min_size then
					return
				end
				
				if y ~= y2 then
					repeat
						local has_exit
						local xi = splitx
						for yi = y+1,y2-1 do
							local prob = random()
							if prob < wall_prob then
								map:setCellFlag(xi,yi, 'terrain', 2)
								map:setCellFlag(xi,yi, 'opaque', true)
							else
								has_exit = true
								map:setCellFlag(xi,yi, 'terrain', 3)
								map:setCellFlag(xi,yi, 'opaque', false)
							end
						end
					until has_exit
				end
				
				splitRoom(x,y, splitx,y2,level+1)
				splitRoom(splitx,y, x2,y2,level+1)
			end
		end
		
	end
	
	-- Initiate with one big room
	for xi = 0,width-1 do
		for yi = 0,height-1 do
			if xi == 0 or yi == 0 or xi == width-1 or yi == height-1 then
				map:setCellFlag(xi,yi, 'terrain', 2)
				map:setCellFlag(xi,yi, 'opaque', true)
			else
				map:setCellFlag(xi,yi, 'terrain', 3)
				map:setCellFlag(xi,yi, 'opaque', false)
			end
		end
	end
	
	splitRoom(0,0,width-1,height-1)
	
	return map
end