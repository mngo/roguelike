local gs         = require 'gamestate'
local terrains   = require 'terrains'
local messages   = require 'messages'
local msg_history= require 'msg_history'

local events = {}

function events:move(object,dx,dy)
	local state       = gs:current()
	local scheduler   = state.scheduler
	local new_x,new_y = object:getPosition()
	new_x,new_y       = new_x + dx,new_y + dy
	
	-- Check for objects
	for i,obj in ipairs(state.map:get(new_x,new_y).objects) do
		if obj ~= object then
			if object.type ~= obj.type then
				return self:attack(object,obj)
			end
		end
	end
	
	-- Check for terrain
	local terrain = object:getMap():getCellFlag(new_x,new_y,'terrain')
	if not (terrain and terrains[terrain].solid) or DEBUG_IGNORE_COLLISION then
		object:move(dx,dy)
	end
	
	object:setTurnOver(true)
end

function events:idle(object)
	object:setTurnOver(true)
end
	
function events:attack(object,target)
	local state = gs:current()
	object:setTurnOver(true)
	self:kill(target)
end

function events:kill(object)
	local state = gs:current()
	msg_history:write(messages[2]:format(object.name))
	if object == gs:current().player then 
		print 'Player died...'
		function state:keypressed(k)
			love.event.push 'quit'
		end
	else
		state:destroy(object)
	end
end

function events:exitFloor(direction)
	local state = gs:current()
	local player= state.player
	local on_stairs = false
	
	local x,y = player:getPosition()
	if state.map:hasTerrain(x,y,'stairs down') and direction == 'down' or 
		state.map:hasTerrain(x,y,'stairs up') and direction == 'up' then
		on_stairs = true
	end
	
	if on_stairs then
		player:setTurnOver(true)
		state.map:removeObject(player)
		state.map.last_player_x = player:getX()
		state.map.last_player_y = player:getY()
		state.map.last_seen_grid= state.last_seen_grid
		state:generateFloor(direction)
	end
end

return events