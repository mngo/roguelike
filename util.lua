return {

	imageToQuads = function(image,rows,columns)
		local list = {}
		local w,h  = image:getWidth(), image:getHeight()
		local qw,qh= w/columns, h/rows
		local code = -1
		
		for i = 0,rows-1 do
			local y = i*qh
			for j = 0,columns-1 do
				code       = code + 1
				local x    = j*qw
				local quad = love.graphics.newQuad(x,y,qw,qh, w,h)
				list[code] = quad
			end
		end
		
		return list
	end,
	
	
	colorHexToRGB = function(hex)
		return math.floor(hex / 0x10000), math.floor(hex / 0x100 % 0x100), hex % 0x100
	end,	
	
	iterateArc = (function()
		local octants = {
			function(x,y) return x,y end,
			function(x,y) return y,x end,
			function(x,y) return -y,x end,
			function(x,y) return -x,y end,
			function(x,y) return -x,-y end,
			function(x,y) return -y,-x end,
			function(x,y) return y,-x end,
			function(x,y) return x,-y end,
		}
		
		return function(x,y,radius,octant)
			-- Based on the wikipedia page about midpoint circle algorithm
			
			local yi,xi = 0,radius
			-- decision variable
			local d     = 1 - radius
			
			local coords= octants[ (octant or 1) ]
			
			return function()
				if xi >= yi then
					local dx,dy = coords(xi,yi)
					
					-- Step in the y direction and decrease x if d > 0
					yi = yi + 1
					if d < 0 then 
						d = d + 2*yi+1
					else
						xi = xi - 1
						d  = d + 2*(yi-xi+1)
					end
					
					return dx,dy,x,y
				end
			end
		end
	end)(),
	
	enum = function(list,first_index)
		local t = {}
		local i = first_index or 1
		for _,name in ipairs(list) do
			t[name] = i
			i       = i + 1
		end
		return t
	end,
}
